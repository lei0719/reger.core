package com.reger.core.model;

import java.io.Serializable;

import com.reger.core.utils.JSONUtils;

public abstract class SysModel implements Serializable {
	public static final long serialVersionUID = 1L;

	public final String toJSONString() {
		return JSONUtils.toJSONString(this);
	}

	public final String toJSONHTML() {
		String model = toJSONString();
		if (model==null||model.isEmpty())
			return null;
		return model.replace("'", "&apos;").replace("\"", "&quot;");
	}
	
	@Override
	public String toString() {
		return this.toJSONString();
	}
}
