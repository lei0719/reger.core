package com.reger.core.model;

import com.reger.core.exception.ExceptionLevel;
import com.reger.core.exception.ExceptionStatus;
import com.reger.core.exception.GlobalException;

public class RegerRelust<T> extends SysModel implements Relust<T> {
	private static final long serialVersionUID = 1L;
	// 返回时间戳
	private final long curtime = System.currentTimeMillis();
	// 返回 错误代码
	private int errorCode;
	private String message;
	private String details;
	private ExceptionLevel level;
	// 返回token
	private String token;
	// 响应中的数据
	private T data;
 
	public final static <T> RegerRelust<T> success() {
		RegerRelust<T> entity = new RegerRelust<>();
		entity.errorCode = ExceptionStatus.SUCCESS;
		entity.message = ExceptionStatus.SUCCESS_MESSAGE;
		return entity;
	}

	public final static <T> RegerRelust<T> success(T date) {
		RegerRelust<T> entity = success();
		entity.data = date;
		return entity;
	}

	public final static <T> RegerRelust<T> success(T date, String token) {
		RegerRelust<T> entity = success(date);
		entity.token = token;
		return entity;
	}

	public final static <T> RegerRelust<T> fail() {
		RegerRelust<T> entity = new RegerRelust<>();
		entity.errorCode = ExceptionStatus.ERROR;
		entity.message = ExceptionStatus.ERROR_MESSAGE;
		return entity;
	}

	public final static <T> RegerRelust<T> fail(int errorCode) {
		RegerRelust<T> entity = fail();
		entity.errorCode = errorCode;
		return entity;
	}

	public final static <T> RegerRelust<T> fail(String message) {
		RegerRelust<T> entity = fail();
		entity.message = message;
		return entity;
	}

	public final static <T> RegerRelust<T> fail(Integer errorCode, String message) {
		RegerRelust<T> entity = fail(errorCode);
		entity.message = message;
		return entity;
	}

	public final static <T> RegerRelust<T> now(GlobalException exception) {
		RegerRelust<T> entity = fail(exception.getCode(), exception.getMessage());
		entity.details = exception.getDetails();
		entity.level = exception.getLevel();
		return entity;
	}

	public final static <T> RegerRelust<T> now(ExceptionStatus status) {
		RegerRelust<T> entity = fail(status.getCode(), status.getMessage());
		entity.details = status.getDetails();
		entity.level = status.getLevel();
		return entity;
	}

	@Override
	public boolean isSucceed() {
		return errorCode == 200;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public ExceptionLevel getLevel() {
		return level;
	}

	public void setLevel(ExceptionLevel level) {
		this.level = level;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public long getCurtime() {
		return curtime;
	}
}
