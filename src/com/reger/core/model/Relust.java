package com.reger.core.model;

import com.reger.core.exception.ExceptionLevel;

public interface Relust<T> {

	boolean isSucceed();

	int getErrorCode();

	String getMessage();

	String getDetails();

	ExceptionLevel getLevel();

	T getData();
}
