package com.reger.core.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class DateUtils {
	public static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

	/**
	 * 获取date当天的开始时间00:00:00
	 * 
	 * @param date
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static final Date initToDayBegin(Date date) {
		if (date == null)
			return null;
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(1);
		return date;
	}

	/**
	 * 获取date当天的结束时间23:59:59
	 * 
	 * @param date
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static final Date initToDayEnd(Date date) {
		if (date == null)
			return null;
		date.setHours(24);
		date.setMinutes(0);
		date.setSeconds(-1);
		return date;
	}

	
	private final static DateFormat[] dateFormats = getDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.S'Z'","yyyy-MM-dd'T'HH:mm:ss",
			"yyyy-MM-dd HH:mm:ss.S", "yyyy-MM-dd HH:mm:ss",
			"yyyy-MM-dd HH:mm", "yyyy-MM-dd", "yyyy-MM", "yyyy/MM/dd HH:mm:ss.S", "yyyy/MM/dd HH:mm:ss",
			"yyyy/MM/dd HH:mm", "yyyy/MM/dd", "yyyy/MM", "yyyy.MM.dd HH:mm:ss.S", "yyyy.MM.dd HH:mm:ss",
			"yyyy.MM.dd HH:mm", "yyyy.MM.dd", "yyyy.MM");
 
	private final static DateFormat[] getDateFormat(final String... patterns) {
		SimpleDateFormat[] dateFormats = new SimpleDateFormat[patterns.length];
		for (int i = 0; i < dateFormats.length; i++) {
			dateFormats[i] = new SimpleDateFormat(patterns[i]);
			dateFormats[i].setLenient(true);
		}
		dateFormats[0].setTimeZone(TimeZone.getTimeZone("GMT 00:00"));
		return dateFormats;
	}
	
	/**
	 * 时间格式化，支持常用的时间格式
	 * @param source
	 * @return
	 */
	public static final Date parse(String source) {
		if (StringUtils.isNotEmpty(source))
			for (DateFormat dateFormat : dateFormats) {
				try {
					return dateFormat.parse(source);
				} catch (Exception e) {
					logger.debug("参数{}转化为日期{}失败", source, dateFormat);
				}
			}
		return null;
	}
	
	/**
	 * 以指定的格式格式化时间
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static final String format(Date date, String pattern) {
		if (StringUtils.isEmpty(pattern))
			return format(date);
		return new SimpleDateFormat(pattern).format(date);
	}
	
	/**
	 * 格式化时间 ，时间格式 yyyy-MM-dd
	 * @param date
	 * @return
	 */
	public static final String format(Date date) {
		if (date == null)
			return null;
		return dateFormats[5].format(date);
	}

	/**
	 * 获取周得第一天
	 * 
	 * @param week
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static final Date getWeekStart(Integer week) {
		Date d = new Date();
		d.setDate(d.getDate() - d.getDay() + (week == null ? 0 : week * 7));
		d.setHours(0);
		d.setMinutes(0);
		d.setSeconds(0);
		return d;
	}

	/**
	 * 获取周得最后一天
	 * 
	 * @param week
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static final Date getWeekEnd(Integer week) {
		Date d = getWeekStart(week);
		d.setDate(d.getDate() + 7);
		d.setSeconds(-1);
		return d;
	}

	private final static SimpleDateFormat ymd = new SimpleDateFormat("y年M月d日");
	private final static SimpleDateFormat md = new SimpleDateFormat("M月d日");
 
	/**
	 * 距离当前的时间差，以刚刚，XX分钟前，XX小时前，昨天，前天，XX天前，X月X日，XXXX年X月X日
	 * 
	 * @param date
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static String beforeNow(Date date) {
		if(date==null)
			return null;
		long between_count = (System.currentTimeMillis()-date.getTime())/ 1000;
		if (between_count < 60)
			return "刚刚";
		if ((between_count = between_count / 60) < 60) {
			return between_count + "分钟前";
		}
		if ((between_count = between_count / 60) < 24) {
			return between_count + "小时前";
		}
		if ((between_count = between_count / 24) == 1) {
			return "昨天";
		}
		if (between_count == 2) {
			return "前天";
		}
		if (between_count < 28) {
			return between_count + "天前";
		}
		if (date.getYear() == new Date().getYear()) {
			return md.format(date);
		} else {
			return ymd.format(date);
		}
	}

}
