package com.reger.core.utils;

public abstract class NumberUtils {
	public static boolean isNumber(String source) {
		if (StringUtils.isEmpty(source))
			return false;
		try {
			Long.parseLong(source);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean isDouble(String source) {
		if (StringUtils.isEmpty(source))
			return false;
		try {
			Double.parseDouble(source);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static Long parseLong(String source) {
		try {
			return Long.parseLong(source);
		} catch (Exception e) {
		}
		return null;
	}

	public static Integer parseInt(String source) {
		try {
			return Integer.parseInt(source);
		} catch (Exception e) {
		}
		return null;
	}

	public static Double parseDouble(String source) {
		try {
			return Double.parseDouble(source);
		} catch (Exception e) {
		}
		return null;
	}

	public static Float parseFloat(String source) {
		try {
			return Float.parseFloat(source);
		} catch (Exception e) {
		}
		return null;
	}
}
