
package com.reger.core.utils;

import java.util.Collection;
import java.util.Map;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.reger.core.exception.GlobalException;

/**
 * 断言类
 * @author leige
 *
 */
public abstract class Assert {

	/**
	 * 如果expression是true,就不抛出异常
	 * @param expression 
	 * @param message 抛出异常的message
	 */
	public static void isTrue(boolean expression, String message) {
		if (!expression) {
			GlobalException.parameterError(message);
		}
	}


	/**
	 * 如果expression是true,就不抛出异常
	 * @param expression 
	 */
	public static void isNull(Object object, String message) {
		if (object != null) {
			GlobalException.parameterError(message);
		}
	}


	/**
	 * 如果object不是null就不抛出异常
	 * @param object
	 * @param message
	 */
	public static void notNull(Object object, String message) {
		if (object == null) {
			GlobalException.parameterError(message);
		}
	}


	/**
	 * text 是空的就抛出异常
	 * @param text
	 * @param message
	 */
	public static void hasLength(String text, String message) {
		if (!StringUtils.hasLength(text)) {
			GlobalException.parameterError(message);
		}
	}

	public static void hasText(String text, String message) {
		if (!StringUtils.hasText(text)) {
			GlobalException.parameterError(message);
		}
	}

	public static void doesNotContain(String textToSearch, String substring, String message) {
		if (StringUtils.hasLength(textToSearch) && StringUtils.hasLength(substring) &&
				textToSearch.contains(substring)) {
			GlobalException.parameterError(message);
		}
	}

	public static void notEmpty(Object[] array, String message) {
		if (ObjectUtils.isEmpty(array)) {
			GlobalException.parameterError(message);
		}
	}

	public static void noNullElements(Object[] array, String message) {
		if (array != null) {
			for (Object element : array) {
				if (element == null) {
					GlobalException.parameterError(message);
				}
			}
		}
	}

	public static void notEmpty(Collection<?> collection, String message) {
		if (CollectionUtils.isEmpty(collection)) {
			GlobalException.parameterError(message);
		}
	}

	public static void notEmpty(Map<?, ?> map, String message) {
		if (CollectionUtils.isEmpty(map)) {
			GlobalException.parameterError(message);
		}
	}


	public static void isInstanceOf(Class<?> clazz, Object obj) {
		isInstanceOf(clazz, obj, "");
	}

	public static void isInstanceOf(Class<?> type, Object obj, String message) {
		notNull(type, "Type to check against must not be null");
		if (!type.isInstance(obj)) {
			GlobalException.parameterError(
					(StringUtils.hasLength(message) ? message + " " : "") +
					"Object of class [" + (obj != null ? obj.getClass().getName() : "null") +
					"] must be an instance of " + type);
		}
	}

	public static void isAssignable(Class<?> superType, Class<?> subType) {
		isAssignable(superType, subType, "");
	}

	public static void isAssignable(Class<?> superType, Class<?> subType, String message) {
		notNull(superType, "Type to check against must not be null");
		if (subType == null || !superType.isAssignableFrom(subType)) {
			GlobalException.parameterError((StringUtils.hasLength(message) ? message + " " : "") +
					subType + " is not assignable to " + superType);
		}
	}

	public static void state(boolean expression, String message) {
		if (!expression) {
			GlobalException.parameterError(message);
		}
	}

	public static void state(boolean expression) {
		state(expression, "[Assertion failed] - this state invariant must be true");
	}

	/**
	 * 如果是null 或者是0就抛出异常
	 * @param num
	 * @param string
	 */
	public static void notZero(Integer num, String message) {
		isTrue(num!=null&&num!=0, message);
	}

	/**
	 * 断言 a大于b, a,b任意一个等于null，或者a<=b就抛出异常
	 * @param a
	 * @param b
	 * @param message
	 */
	public static <A extends Comparable<B>,B extends Comparable<A>> void isGreater(A a,B b, String message) {
		isTrue(a!=null&&b!=null&&a.compareTo(b)>0, message);
	}
	
	/**
	 * 断言 a b相等 ,  a b不相等就抛出异常
	 * @param a
	 * @param b
	 * @param message
	 */
	public static void isEquals(Object a,Object b, String message) {
		isTrue((a==null&&b==null)||(a!=null&&a.equals(b)), message);
	}

	/**
	 * 断言 a b不相等 ,  a b相等就抛出异常
	 * @param a
	 * @param b
	 * @param message
	 */
	public static void notEquals(Object a,Object b, String message) {
		isTrue((a==null&&b!=null)||(a!=null&&!a.equals(b)), message);
	}
}
