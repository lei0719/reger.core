package com.reger.core.exception;

public enum ExceptionLevel {
	ERROR, WARN, INFO, DEBUG, TRACE, OFF;
}