package com.reger.core.exception;

public enum GlobalExceptionStatus implements ExceptionStatus {
	
	OK(ExceptionLevel.DEBUG,0, "操作成功"), 
	PAGE_NOT_FOUND(ExceptionLevel.INFO,-2, "请求资源不存在"),
	FAIL(ExceptionLevel.ERROR,-1, "操作失败"), 
	USER_NOT_LOGIN(ExceptionLevel.INFO,-10, "用户未登录"),
	ACCESS_DENIED(ExceptionLevel.INFO,-2, "访问被拒绝"),
	FAILED_TO_SAVE_DATA(ExceptionLevel.WARN,-2, "保存数据到数据库失败"),
	NO_ID_FOUND(ExceptionLevel.INFO,-2, "没有找到该编号的数据"),
	PARAMETER_ERROR(ExceptionLevel.INFO,-2, "参数错误"),  
	WARN(ExceptionLevel.WARN,-2, "警告"), 
	INFO(ExceptionLevel.INFO,-2, "提示"), 
	VALID_FAIL(ExceptionLevel.INFO,710, "参数校验失败"),  ;

	
	private String message, Details;
	private int code;
	private ExceptionLevel level;

	GlobalExceptionStatus(int code, String message) {
		this.message = message;
		this.code = code;
		this.level = ExceptionLevel.WARN;
	}

	GlobalExceptionStatus(int code, String message, String cnmsg) {
		this(code, message);
		this.Details = cnmsg;
	}

	GlobalExceptionStatus(ExceptionLevel level, int code, String message) {
		this.message = message;
		this.code = code;
		this.level = level;
	}

	GlobalExceptionStatus(ExceptionLevel level, int code, String message, String cnmsg) {
		this(level, code, message);
		this.Details = cnmsg;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCnmsg() {
		return Details;
	}

	public void setCnmsg(String cnmsg) {
		this.Details = cnmsg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public ExceptionLevel getLevel() {
		return level;
	}

	public void setLevel(ExceptionLevel level) {
		this.level = level;
	}

	@Override
	public String getDetails() {
		// TODO Auto-generated method stub
		return null;
	}
}
