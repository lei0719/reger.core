package com.reger.core.exception;

public interface ExceptionStatus {

	int SUCCESS = 200;
	String SUCCESS_MESSAGE = "操作成功";
	int ERROR = 500;
	String ERROR_MESSAGE = "操作失败";

	int getCode();

	String getMessage();

	String getDetails();

	ExceptionLevel getLevel();

}
