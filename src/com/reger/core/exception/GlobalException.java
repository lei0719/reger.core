package com.reger.core.exception;

/**
 * 业务异常类.
 * 
 * @author Administrator
 *
 */
public class GlobalException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private int code;
	private String message;
	private String details;
	private ExceptionLevel level;

	public GlobalException() {
	}

	private void init(ExceptionStatus exceptionStatus) {
		this.code = exceptionStatus.getCode();
		this.message = exceptionStatus.getMessage();
		this.details = exceptionStatus.getDetails();
		this.level = exceptionStatus.getLevel();
	}

	public GlobalException(ExceptionStatus exceptionStatus) {
		this.init(exceptionStatus);
	}

	public GlobalException(ExceptionStatus exceptionStatus, String details) {
		this.init(exceptionStatus);
		this.details = details;
	}

	public GlobalException(Throwable throwable, ExceptionStatus exceptionStatus) {
		super(throwable);
		this.init(exceptionStatus);
	}

	public final static void parameterError(final String details) {
		throw new GlobalException(GlobalExceptionStatus.PARAMETER_ERROR, details);
	}

	public final static void error(final String details) {
		throw new GlobalException(GlobalExceptionStatus.FAIL, details);
	}

	public final static void warn(final String details) {
		throw new GlobalException(GlobalExceptionStatus.WARN, details);
	}

	public final static void info(final String details) {
		throw new GlobalException(GlobalExceptionStatus.INFO, details);
	}

	@Override
	public String toString() {
		StringBuffer message = new StringBuffer();
		if (this.level != null)
			message.append("  异常级别:").append(this.level);
		message.append("  异常代码:").append(this.code);
		if (this.message != null)
			message.append("  原始描述:").append(this.message);
		if (this.details != null)
			message.append("  详细描述:").append(this.details);
		return message.toString();
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public ExceptionLevel getLevel() {
		return level;
	}

	public void setLevel(ExceptionLevel level) {
		this.level = level;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
